const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [ true, 'firstName is required.']
	},
	lastName: {
		type: String,
		required: [ true, 'lastName is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required.']
	}


	
})

module.exports = mongoose.model('User', user_schema)

