//IMPORTS
const Product = require('../models/Product')
const User = require('../models/User')
const Order = require('../models/Order')


const auth = require('../auth')


//Create Product
module.exports.createProduct =(data) => {
	if(data.isAdmin){
		let new_product = new Product({
			name:  data.product.name,
			description: data.product.description,
			price: data.product.price

		})
		return new_product.save().then((new_product, error)=>{
			if(error){
				return false
			}
			return {
				message: 'New Product successfully created!'
			}
		})

	}
	//return Promise.resolve('User must be admin')-- 
	let message = Promise.resolve({
		message: 'User must be admin to access this.'
	})

	return message.then((value)=>{
		return value
	})
}

//Retrieve all products
module.exports.allProducts =() => {
		return Product.find({}).then((result)=>{
			return result
		})
}

//Retrieve single product
module.exports.getProduct = (product_Id) => {
	return Product.findById(product_Id).then((result)=>{
		return result
	})
}

//update my product
module.exports.updateProduct = (product_Id, data) => {
	if(data.isAdmin) {
		return Product.findByIdAndUpdate(product_Id, {
			name: data.name,
			description: data.description,
			price: data.price
		}).then((updated_products, error) => {
			if (error) {
				return false
			}
			return {
				message: 'Product updated successfully'
			}
		})
	}
		let message = Promise.resolve ({message:'Request for admin access to continue'})
			return message.then((value)=>{
				return value
			})
}

//Archive my product
module.exports.isActiveProduct = (product_Id, data) => {
	if(data.isAdmin){
	return Product.findByIdAndUpdate(product_Id, {isActive:false}).then((archive, error) =>{
			if (error) {
				return false
			}
			return {message: 'Product archived'}
		})
	}		
	let message = Promise.resolve ({message:'Request for admin access'})
			return message.then((value)=>{
				return value
			})
}
//retrieve archived my product
module.exports.isActiveRetrieved = (product_Id, data) => {
	if(data.isAdmin){
	return Product.findByIdAndUpdate(product_Id, {isActive:true}).then((archive, error) =>{
			if (error) {
				return false
			}
			return {message: 'Product retrieved'}
		})
	}		
	let message = Promise.resolve ({message:'Request for admin access'})
			return message.then((value)=>{
				return value
			})
}

