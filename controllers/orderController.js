//IMPORTS
const Order = require('../models/Order')

//create order (user)
module.exports.createOrder =(data) => {
	if(!data.isAdmin){
		let new_order = new Order({
			userId:  data.userId.userId,
			product_id: data.product_id.product_id,
			quantity: data.quantity.quantity,
			totalAmount: data.totalAmount.totalAmount

		})
		return new_order.save().then((new_order, error)=>{
			if(error){
				return false
			}
			return {
				message: 'New Order successfully created!'
			}
		})
	}
	let message = Promise.resolve({
		message: 'This feature is for users only.'
	})

	return message.then((value)=>{
		return value
	})
}

//retrieve all Orders
module.exports.allOrders =(data) => {
	if (data.isAdmin) {
		return Order.find({}).then((result)=>{
			return result
		})
	}
	let message = Promise.resolve ({message:'Request for admin access'})
			return message.then((value)=>{
				return value
			})
		
}

//Retrieve order
module.exports.retrieveOrder = (userId, data) => {
	if (!data.isAdmin) {
		return Order.find(userId).then((result)=>{
		return result
	})
}
	let message = Promise.resolve ({message:'For NON-admin only'})
			return message.then((value)=>{
				return value
			})
	
}


