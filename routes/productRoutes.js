const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')


//create product (admin only)
	router.post('/create', auth.verify, (request, response)=>{
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	productController.createProduct(data).then((result)=>{
		response.send(result)
	})
	
})


//retrieve all product
router.get('/', (request, response)=>{

	productController.allProducts().then((result)=>{
		response.send(result)
	})
})


//retrieve single product
router.get('/:productId', (request, response) => {
	productController.getProduct(request.params.productId).then((result)=>{
		response.send(result)
	})
})


//update my product (admin only)
router.patch('/:productId/update', auth.verify, (request, response)=>{
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	productController.updateProduct(request.params.productId, data).then((result)=>{
		response.send(result)
	})
})

//Archive my product
router.patch('/:productId/archive', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	productController.isActiveProduct(request.params.productId, data).then((result)=>{
		response.send(result)
	})
})

//Retrieve Archive my product
router.patch('/:productId/retrieveArchive', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	productController.isActiveRetrieved(request.params.productId, data).then((result)=>{
		response.send(result)
	})
})
module.exports = router