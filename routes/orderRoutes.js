const express = require('express')
const router = express.Router()
const orderController = require('../controllers/orderController')
const auth = require('../auth')

module.exports = router

//create order
	router.post('/create', auth.verify, (request, response)=>{
	const data = {
		userId: request.body,
		product_id: request.body,
		quantity: request.body,
		totalAmount: request.body,
		products:request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
		
	}
	orderController.createOrder(data).then((result)=>{
		response.send(result)
	})
	
})

//retrieve all orders
router.get('/', auth.verify, (request, response)=>{
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	orderController.allOrders(data).then((result)=>{
		response.send(result)
	})
})


//retrieve order
router.get('/:orderId', auth.verify, (request, response) => {
		const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}
	orderController.retrieveOrder(request.params.userId, data).then((result)=>{
		response.send(result)
	})
})

